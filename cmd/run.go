// Copyright © 2016 NAME HERE <EMAIL ADDRESS>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"log"
	"time"

	"github.com/spf13/cobra"
	"github.com/tarm/serial"
	"gitlab.com/creator-makerspace/go-rs485-nodeproto"
)

var logRx bool
var rxePin int
var txePin int
var rxTimeout int

func logStatus(status string) {
	if logRx {
		log.Printf("RX: %s", status)
	}
}

// runCmd represents the run command
var runCmd = &cobra.Command{
	Use:   "run",
	Short: "A brief description of your command",
	Long: `A longer description that spans multiple lines and likely contains examples
and usage of using your command. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
	Run: func(cmd *cobra.Command, args []string) {
		serialConfig := &serial.Config{
			Name:        serialPort,
			Baud:        9600,
			ReadTimeout: 20 * time.Millisecond,
		}

		serialPort, err := serial.OpenPort(serialConfig)
		if err != nil {
			log.Fatal(err)
		}

		opts := nodeproto.GPIOOptions{
			RxEnablePin: rxePin,
			TxEnablePin: txePin,
		}

		ctrl := nodeproto.NewRaspberryIOCtrl(serialPort, opts)
		ctrl.Init()
		nc := nodeproto.NewController(serialPort, ctrl)

		var rxStatus nodeproto.RecvStatus

		for {
			status := nc.Receive(rxTimeout)

			if err != nil {
				log.Print(err)
				continue
			}

			if rxStatus != status {
				switch status {
				case nodeproto.RecvIdle:
					logStatus("IDLE")
				case nodeproto.RecvDataReady:
					asString := string(nc.Packet.GetData())
					log.Printf("Received: %s", asString)
				case nodeproto.RecvBusy:
					logStatus("BUSY")
				}
			}
		}
	},
}

func init() {
	RootCmd.AddCommand(runCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// runCmd.PersistentFlags().String("foo", "", "A help for foo")

	runCmd.PersistentFlags().BoolVar(&logRx, "logRx", false, "Log RX status")
	runCmd.PersistentFlags().IntVar(&rxePin, "rxePin", 17, "RX enable pin")
	runCmd.PersistentFlags().IntVar(&txePin, "txePin", 18, "TX enable pin")
	runCmd.PersistentFlags().IntVar(&rxTimeout, "rxTimeout", 500, "RX timeout")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// runCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")

}
